# Jitsi

# Dev

Install ansible

``` bash
virtualenv --python=/usr/bin/python3 ~/venv/vagrant
source ~/venv/vagrant/bin/activate
pip install ansible==2.10.3
ansible-galaxy collection install community.docker
```

Install virtualbox and vagrant

``` bash
sudo apt install virtualbox vagrant
```

Run deployement on debian buster:

``` bash
vagrant up
# see also
# vagrant provision
# vagrant destroy 
```

Test jitsi:

go to http://192.168.33.11:8000/


# Deploy on a remote server

## Create an `inventory-<env>` file

``` text
[<env>]
<remote-ip>
```

## Create an `playbook-<env>.yml` file

Just copy/paste the example playbook, and edit the `hosts`:

``` yaml
- hosts: <env>
  become: yes
#  [...]
```

## Deploy

``` bash
ansible-playbook playbook-<env>.yml -i inventory-<env> --user <your remote user> --extra-vars "ansible_sudo_pass=<the password>"
```


# Biblio
- [basic jitsi install](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker)
- [scalable jitsi](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-scalable)
- [ethibox](https://github.com/ethibox/stacks) avec les [details de l'architecture](https://johackim.com/auto-hebergement/infrastructure-hebergeur-web-independant/)
- [docker jitsi meet](https://github.com/jitsi/docker-jitsi-meet)
- [mesurer l'utilisation de jitsi](https://blog.zwindler.fr/2020/06/08/superviser-votre-instance-jitsi-avec-prometheus-et-grafana/)

certbot:
- [certbot install script](https://github.com/jitsi/jitsi-meet/blob/master/resources/install-letsencrypt-cert.sh)


